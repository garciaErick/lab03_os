/**
 * @author   Erick Garcia egarcia87@miners.utep.edu
 * @desc     This class will will write the list of prime numbers 
 *  that were processed by threadx to primesx and merge these files
 *  into a final output primest containing all prime numbers
 * @required egarcia_prime.h
 */

#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include "egarcia_prime.h"

/* Global variables that are shared */
sPRIME_THREAD primeThreadData[MAX_THREADS];

/**
 * Func prime_search()
 * @desc   Find out primes with an upper bound and 
 *  lowebound. It compute this by using two threads, 
 *  and writing the output of each thread into a file
 *  prime(nThread number) and merging these files into a file 
 *  called primest.
 * @param  *param -  a void pointer pointing to a sPRIME_THREAD
 */
void *prime_search(void *param) {
  /**
   * We have to first grab a hold of our void pointer 
   * pointing to our sPRIME_THREAD
   */
  sPRIME_THREAD *primes = (sPRIME_THREAD *) param;

  /* Open our two files for writing with the w flag*/
  FILE *file1, *file2;           //primesx
  unsigned int tid1 = primeThreadData[0].num;
  file1 = fopen("primes1", "w"); //primes1
  file2 = fopen("primes2", "w"); //primes2

  /* Run our test_prime algorithm */
  int i;
  for(i = primes->low; i < (int) primes->high; i++)
    if(test_prime(i) == 1){
      primes->current = i;    //Getting which thread we are in
      if(tid1 == primes->num) //Write to primes1
        fprintf(file1,"%d\n", primes->current);
      else                    //Write to primes2
        fprintf(file2,"%d\n", primes->current);
    }
  fclose(file1);              //close primes1
  fclose(file2);              //close primes2
  return NULL;
}

void *mini_shell(void *param) {
  int inputSize, i;
  int threadNum;
  char buff[128];		// Character buffer

  while(1) {
    // Request a line of input
    write(1, " -> ", 4);
    // Read the line
    inputSize = read(0, buff, 128);
    // Strip off carriage return
    buff[inputSize-1] = '\0';

    if((buff[0] >= '1') && (buff[0] <= '9')) {
      // An integer was entered
      threadNum = buff[0] - '0';
      if(threadNum <= numThreads) {
        printf("Thread %d progress: %d\n", threadNum, primeThreadData[threadNum-1].current);
      }
      else {
        printf("Thread %d does not exist\n", threadNum);
      }

    }
    else 		
      if(buff[0] == 'a') // Print status for all threads
        for(i = 0; i < numThreads; i++) 
          printf("Thread %d progress: %d\n", i+1, primeThreadData[i].current);
      else 
        printf("Unrecognized input\n");
    printf("\n");
    fflush(NULL);
  }
  pthread_exit(0);
}

/**
 * Func test_prime()
 * @desc   determine if a number is a prime number
 * @param  n - number to determine if it is prime
 * @return 1 if is prime, else 0
 */
int test_prime(int n) { 
  int i, flag;
  i    = 0;
  flag = 0;

  /**
   * Dividing number by two because if you can get this number by multiplying a number
   * by two then its not a prime number, afterwards it keeps doing division by all numbers
   * with a remainder to check if its a prime number
   */
  for(i = 2; i <= n/2 ; i++) 
    if (n%i == 0) { // Not a prime number, exit out of loop
      flag = 1;
      break;
    }

  if(flag == 0) // Is prime number
    return 1;
  else          // Not a prime number
    return 0;

}

/**
 * Func balanceLoad()
 * @desc   Balance load between 2 threads by 70 -30
 * @param  lowerBound - starting number
 * @param  upperBound - final number
 * @return the middle number that will split load 
 *  into 70 - 30 
 */
int balanceLoad(int lowerBound, int upperBound){
  int result =  (upperBound - lowerBound) * 0.70;
  result += lowerBound;
  return result;
}
