/**
 * @author   Erick Garcia <egarcia87@miners.utep.edu>
 * @desc     This class will accept two inputs starting number
 * and final number, and will determine which prime numbers exist 
 * in this range using threads to make the computation faster, and enable 
 * interactivity by using a minishell in another thread. 
 * @required egarcia_prime.h
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include "egarcia_prime.h"

#define BUFFER_SIZE	1000000

unsigned char buffer[BUFFER_SIZE+1];
unsigned char fileName[100];
FILE *primeFile;
FILE *primeThreadFile;


int main(int argc, char *argv[]){
  /**
   * This will tell the user how to run our program if run without 
   * arguments or less than the required ones and exit.
   */
  if(argc < 2){
    printf("Usage: egarcia_lab3 [starting number] [final number]\n");
    return 0;
  }

  int i, bytesRead, bytesWritten;
  pthread_t tid[MAX_THREADS]; 
  pthread_t tidshell;
  pthread_attr_t attr;
  time_t before, after;

  /**
   * By trial and error I came to the conclusion that the optimal 
   * load in this case was 70 - 30 because with a smaller number
   * the computation is a lot faster than with a larger number.
   * using 70 - 30 makes both threads finish more likely at the 
   * same time
   */
  int middle = balanceLoad(atof(argv[1]),atof(argv[2]));

  /* Dividing the range between the two threads */
  // Thread 1
  primeThreadData[0].num  = tid[0];        //Thread number
  primeThreadData[0].low  = atoi(argv[1]); //Lower bound
  primeThreadData[0].high = middle;        //Upper bound
  // Thread 2
  primeThreadData[1].num  = tid[1];        //Thread numbe
  primeThreadData[1].low  = middle+1;      //Lower bound
  primeThreadData[1].high = atoi(argv[2]); //Upper bound

  /* Record time at start */
  before = time(NULL);

  /* Setup threads to find prime numbers */
  pthread_attr_init(&attr);
  numThreads = 2;

  /* Assign our two threads to the prime_search function */
  pthread_create(&tid[0], &attr, prime_search, &primeThreadData[0]);
  pthread_create(&tid[1], &attr, prime_search, &primeThreadData[1]);

  /* Setup a mini shell thread to provide interactivity with the user */
  pthread_create(&tidshell,&attr,mini_shell,NULL);

  /* Create primes output file */
  primeFile = fopen("primest","w");
  fclose(primeFile);


  /* Wait for the prime search threads to complete and combine their data */
  for(i = 0; i < numThreads; i++) {
    /* Wait for the next thread to complete */
    pthread_join(tid[i],NULL);
    /* On thread completion, write its data to "primest" */
    fileName[0] = '\0';
    sprintf(fileName, "primes%d", i+1);					// Open the thread's data file
    if((primeThreadFile = fopen(fileName,"r")) == NULL) {
      printf("Failed to open file: %s\n", fileName);
    }
    else {
      if((primeFile = fopen("primest","a")) == NULL){ // Open "primest" 
        printf("Failed to open file: primest\n");
      }
      else {
        while(feof(primeThreadFile) == 0) {
          /* Read from the thread's data file */
          bytesRead = fread(buffer,1,BUFFER_SIZE,primeThreadFile);
          /* Write to the combined file */
          bytesWritten = fwrite(buffer,1,bytesRead,primeFile);
        }
        fclose(primeFile);
      }
      fclose(primeThreadFile);
    }
  }

  /* Record execution time */
  after = time(NULL);
  printf("\nPrime search done after %ld seconds\n", after-before);

  /* Lastly, kill the interaction thread */
  pthread_kill(tidshell, SIGKILL);
}
